
FROM debian:jessie

COPY . /app

RUN apt-get update

RUN apt-get install python -y

RUN apt-get install python-pip -y

RUN pip install Flask

RUN pip freeze > requirements.txt 

RUN python -v

CMD ["python", 'app.py']




#FROM python:3-alpine

#COPY app /app

#RUN pip freeze > requirements.txt 
#pip install -r requirements.txt
#RUN pip install Flask

#CMD ["python", 'app.py']