from flask import Flask
import docker

app = flask(__name__)

@app.route ('/')
def index ():
    return "<h1>this is home page</h1>"

@app.route ('/containers/')
def get_containers():
    client = docker.from_env()
    containers =  client.containers.list()
    return containers

@app.route ('/containers/{id}')
def get_container(id):
    client = docker.from_env()
    container =  client.containers.get(id)
    return container

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")